import webapp
import re
import urllib.parse


class shortenerApp (webapp.webApp):

    content = {}

    def short_urls(self):
        inf = "Lista:<br>"
        for url in self.content:
            inf = inf +self.content[url] + " --> " + url + '<br>'
        return inf


    def parse(self, request):
        metodo = request.split(' ')[0]
        recurso = request.split(' ')[1]
        cuerpo = request.split('\r\n\r\n')[1]
        return metodo,recurso,cuerpo

    def process(self, parsedRequest):
        metodo, ResourceName, cuerpo = parsedRequest
        print(ResourceName)
        if metodo == "POST":
            url = urllib.parse.unquote(cuerpo.split('&')[0].split('=')[1])
            short = cuerpo.split('&')[1].split('=')[1]

            if not url and not short: # si estan  vacias los campos devuelve error
                statusCode = "404 not Found"
                body = "<h1>Error</h1>Debes rellenar los dos campos"

            else:
                if not re.match("^https?://",url): # Mira si url empieza por http(s)
                    url = "https://" + url

                self.content[short] = url # añado al diccionario
                # Imprime la url y la short
                statusCode = "200 OK"
                body = f"<html><body>url: <a href='{url}'>{url}</a><br>Short: <a href='{short}'>{self.content[short]}</a></body></html>"

        if metodo == "GET":
            # Esto es el GET
            if ResourceName == "/":
                statusCode = "200 OK"
                form = '<form action="" method="POST"><p>Introduce la url: <input type="text" name="url"/>url acortada: <input type="text" name="short"/><input type="submit" value="Enviar url"/></form>'
                body = f"<html><body>{form} URLS ya acortadas: <p>{self.short_urls()}</p></body></html>"
            else:
                ResourceName = ResourceName[1:] # te quita la barra del principio porque lo guardo en el diccionario sin barra
                if ResourceName in self.content: # Si el recurso se encuentra en nuestro diccionario,que nos redirija
                    statusCode = "302 Found"
                    body = f"<html><body><meta http-equiv='refresh' content='0;url={self.content[ResourceName]}'></body></html>"
                else:
                    statusCode = "404 Not Found"
                    body = f"<html><body><p>Recurso: {ResourceName} no disponible</body></html>"
        return(statusCode, body)


if __name__ == "__main__":
    testWebApp = shortenerApp("localhost", 54321)